DROP TABLE IF EXISTS  Post

CREATE TABLE Post (
    Id int IDENTITY(1,1) PRIMARY KEY,
    Autor NVarchar(100),
    Titulo NVarchar(100) NOT NULL,
    Fecha NVarchar(100) NOT NULL,
    Categoria NVarchar(100) NOT NULL,
)


CREATE TRIGGER CategoriaDefecto
ON Post
FOR INSERT
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @categoria VARCHAR(100) = 'CategoriaPorDefecto'
	declare @autor VARCHAR(100) = 'AutorPorDefecto'

	update Post set Autor=@autor where Categoria='null'
	update Post set Categoria=@categoria where Categoria='null'
	
END
