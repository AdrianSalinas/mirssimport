package com.cesur;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

public class App {
	
    private static String serverDB="localhost";
    private static String portDB="1533";
    private static String DBname="Blog";
    private static String userDB="sa";
    private static String passwordDB="12345Ab##";
    /** 
     * @param args
     * @throws ParseException 
     */
    public static void main( String[] args ) throws ParseException
    {
    	Scanner sc=new Scanner(System.in);
    	Scanner sc2=new Scanner(System.in);
    	Scanner sc3=new Scanner(System.in);
    	Scanner sc4=new Scanner(System.in);
    	Scanner sc5=new Scanner(System.in);
    	Scanner sc6=new Scanner(System.in);
    	
        CrearBBDD();
        //Proyecto final de DAW
        
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);

//        String ret = b.leerBBDDUnDato("select max(id) from Clientes");
//        b.modificarBBDD("delete from Clientes where id =" + ret);

        
        XmlParse x =new XmlParse();

        String url = "";
        
    	List<String>  titulos = null;
        
        List<String>  autores = null;
        
        List<String>  categoria = null;
        
        List<String>  fecha = null;
        
        ArrayList <Date> fecha_copia = new ArrayList<>();
        
        List<Date> fecha_copia2 = null;
        
        int opcion;
        String URL_RSS;
        int respuesta=0;
        
        
        do {
        System.out.print("\n0: Importar RSS \n1: Ver ultimos 10 post \n2: Ver todos los post\n3: Buscar por titulo o contenido una palabra \n4: Filtrar por categoria \n5: Buscar por ID \nEliga la opcion que desee: ");
        opcion=sc.nextInt();
        switch(opcion) {
        
        case 0:  //Importar RSS
        	System.out.print("Indroduce la URL: ");
        	URL_RSS=sc2.nextLine();
        	
        	url=URL_RSS;
        	
        	titulos = x.leer("//item/title/text()",url);
            
           autores = x.leer("//item/creator/text()",url);
            
           categoria = x.leer("//item/category/text()",url);
            
           fecha = x.leer("//item/pubDate/text()",url);
            
            for (int i = 0; i < titulos.size(); i++) {
            	b.modificarBBDD("insert into Post values ('"+autores.get(i)+"','"+titulos.get(i)+"','"+fecha.get(i)+"','"+categoria.get(i)+"')");
                 
            }
            
            System.out.print("Desea continuar? 1 para si: ");
           respuesta=sc4.nextInt();
           
        	break;
        	
        case 1:
        	
        	DateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
        	Date date = null;
        	
        	for (int i = 0; i < titulos.size(); i++) {
        		
        		date = formatter.parse(fecha.get(i));
        		fecha_copia.add(date);
        		
        		
        		
        		
            }
        	Collections.sort(fecha_copia);
        	
        	if(fecha_copia.size()>=10) {
        		
        		fecha_copia2 = fecha_copia.subList((fecha_copia.size()-10), fecha_copia.size());
        		
        		System.out.println(fecha_copia2);
        		
        	}else {
        		System.out.println(fecha_copia);
        	}
        	
        	
        	
        	break;
        	
        case 2:     // Ver todos los post
        	
        	
        	for (int i = 0; i < titulos.size(); i++) {
                 System.out.println(("Titulo:   " + titulos.get(i)+ ", Fecha:   " + fecha.get(i)));
                 
            }
        	
        	System.out.print("Desea continuar? 1 para si: ");
            respuesta=sc4.nextInt();
        	
        	break;
        case 3:
        	System.out.print("Dime la palabra por la que buscar: ");
        	String palabra;
        	palabra=sc3.nextLine();
        	
        	for (int i = 0; i < titulos.size(); i++) {
        		
        		if(titulos.get(i).toLowerCase().contains(palabra.toLowerCase())) {
        			 System.out.println(("Autor: "+autores.get(i)+", Titulo:   " + titulos.get(i)+ ", Fecha:   " + fecha.get(i)));	
        		}
        		
            }
        	
        	
        	System.out.print("Desea continuar? 1 para si: ");
            respuesta=sc4.nextInt();
        	break;
        case 4:
        	System.out.println("Estas son todas las categorias que hay: ");
        	String comprobar="";
        	
        	for (int i = 0; i < titulos.size(); i++) {
        		
        		
        		if(!comprobar.equals(categoria.get(i))) {
        			System.out.println(categoria.get(i));
        		}
        		comprobar=categoria.get(i);

            }
        	
        	System.out.print("Dime la categoria por la que buscar: ");
        	String categoria2;
        	categoria2=sc5.nextLine();
        	
        	 for (int i = 0; i < titulos.size(); i++) {
        		 
        		 if(categoria.get(i).toLowerCase().contains(categoria2.toLowerCase())) {
        			 System.out.println(("Autor: "+autores.get(i)+", Titulo:   " + titulos.get(i)+ ", Fecha:   " + fecha.get(i)));	
        		}
        		 
             }
        	
        	System.out.println("Desea continuar? 1 para si: ");
            respuesta=sc4.nextInt();
        	
        	break;
        	
        case 5:
        	
        	System.out.print("Dime el id del post que quieras ver: ");
        	int idpost;
        	idpost=sc6.nextInt();
        	
        	b.leerBBDD("SELECT * FROM POST WHERE ID = " +idpost, 5);
        	
        
        	System.out.println("Desea continuar? 1 para si: ");
            respuesta=sc4.nextInt();
            
        	break;
        	
        	
        default: System.out.println("Tenga un buen dia");
        	
        }
       }while(respuesta == 1);
        
        System.out.println("Tenga un buen dia");
 }
    
    


    private static void CrearBBDD() {
    	
        bbdd b = new bbdd(serverDB,portDB,"master",userDB,passwordDB);
        
        String i= b.leerBBDDUnDato("SELECT count(1) FROM sys.databases  where name='"+DBname+"' ");

        if (i.equals("0")){
            b.modificarBBDD("CREATE DATABASE " + DBname);
            b=new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
            String query = LeerScriptBBDD("scripts/CrearBBDD.sql");
            b.modificarBBDD(query);
      
        }
        
    }

    private static String LeerScriptBBDD(String archivo) {
       String ret = "";
        try {
            String cadena;
            FileReader f = new FileReader(archivo);
            BufferedReader b = new BufferedReader(f);
            while((cadena = b.readLine())!=null) {
                ret =ret +cadena;
            }
            b.close();
        } catch (Exception e) {
        }
       return ret;

    }
}
